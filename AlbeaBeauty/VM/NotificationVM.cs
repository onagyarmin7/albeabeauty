﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;

namespace AlbeaBeauty.VM
{
    class NotificationVM : ViewModelBase
    {
        private List<string> intervals = new List<string>() { "3 hónapon belül", "6 hónapon belül", "1 éven belül" };
        public List<string> Intervals { get { return intervals; } }
    }
}

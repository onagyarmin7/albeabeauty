﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows.Input;
using static AlbeaBeauty.Utils.StructCollection;

namespace AlbeaBeauty.VM.TreatmentHandlers
{
    public class TreatmentSettingsVM : ViewModelBase
    {
        private ObservableCollection<string> treatmentList = new ObservableCollection<string>();
        public ObservableCollection<string> TreatmentList
        {
            get
            {
                if(treatmentList.Count < 1)
                {
                    treatmentList.Add("asdka");
                    treatmentList.Add("asd");
                    treatmentList.Add("asd5221");
                    treatmentList.Add("asd512");
                }

                return treatmentList;
            }
            set { Set(ref treatmentList, value); }
        }

        private ObservableCollection<string> orderList = new ObservableCollection<string>() { "1", "2", "3", "4" };
        public ObservableCollection<string> OrderList
        {
            get
            {
                return orderList;
            }
        }

        public ICommand DragCommand;

        public TreatmentSettingsVM()
        {
            DragCommand = new RelayCommand<DragNDropInfo>((info) =>
            {
                if (info.removedIdx < info.targetIdx)
                {
                    TreatmentList.Insert(info.targetIdx + 1, info.droppedData);
                    TreatmentList.RemoveAt(info.removedIdx);
                }
                else
                {
                    int remIdx = info.removedIdx + 1;
                    if (TreatmentList.Count + 1 > remIdx)
                    {
                        TreatmentList.Insert(info.targetIdx, info.droppedData);
                        TreatmentList.RemoveAt(remIdx);
                    }
                }
            });
        }
    }
}

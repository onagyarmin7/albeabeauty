﻿using AlbeaBeauty.View.TreatmentHandlers;
using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Controls;

namespace AlbeaBeauty.VM.TreatmentHandlers
{
    class TreatmentManagerVM : ViewModelBase
    {
        private UserControl startWindow;
        public UserControl StartWindow
        {
            get 
            { 
                if(startWindow == null)
                {
                    var window = new TreatmentList();
                    window.OpenTreatmentSetter += () => { CurrentWindow = TreatmentSettingsWindow; };
                    startWindow = window;
                }
                return startWindow;
            }
        }

        private UserControl treatmentSettingsWindow;
        public UserControl TreatmentSettingsWindow
        {
            get 
            {
                if (treatmentSettingsWindow == null)
                {
                    var window = new TreatmentSettings();
                    window.GoBack += () => { CurrentWindow = StartWindow; };
                    treatmentSettingsWindow = window;
                }

                return treatmentSettingsWindow;
            }
        }

        private UserControl currentWindow;
        public UserControl CurrentWindow
        {
            get { return currentWindow; }
            set { Set(ref currentWindow, value); }
        }

        public TreatmentManagerVM()
        {
            CurrentWindow = StartWindow;
        }
    }
}

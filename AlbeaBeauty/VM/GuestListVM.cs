﻿using AlbeaBeauty.Logic.GuestList;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using static AlbeaBeauty.Utils.StructCollection;

namespace AlbeaBeauty.VM
{
    class GuestListVM : ViewModelBase
    {
        public ICommand AddUser;
        private IGuestListLogic logic;

        public GuestListVM(IGuestListLogic logic)
        {
            AddUser = new RelayCommand<AddUserStruct>((userInfo) =>
            {
                logic.AddNewGuest(userInfo.panel, userInfo.data, userInfo.id);
            });        
        }
    }
}

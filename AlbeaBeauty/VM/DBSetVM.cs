﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using AlbeaBeauty.Logic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using AlbeaBeauty.Logic.Database;
using AlbeaBeauty.Utils;

namespace AlbeaBeauty.VM
{
    class DBSetVM : ViewModelBase
    {
        private string state;
        public string State
        {
            get { return state; }
            set 
            { 
                Set(ref state, value);
            }
        }
        public event Action DatabasePosChangedEvent;

        public ICommand ChangeDatabase;
        public ICommand BrowseFile;
        public ICommand CreateNewDB;

        private const string Icon_Checkmark = "../Resources/checkmark.png";
        private const string Icon_X = "../Resources/x.png";

        private IDBLogic logic;

        private Nullable<bool> dbset;
        public Nullable<bool> IsDatabaseSet { get; set; }

        public DBSetVM(IDBLogic logic)
        {
            this.logic = logic;
            RefreshCheckmarkState();

            DatabasePosChangedEvent += DatabaseStateChanged;

            BrowseFile = new RelayCommand(() =>
            {
                if(this.logic.SetDatabase(this.logic.BrowseFile()))
                {
                    DatabasePosChangedEvent?.Invoke();
                }
            });

            CreateNewDB = new RelayCommand(() =>
            {
                if(this.logic.CreateDB())
                {
                    DatabasePosChangedEvent?.Invoke();
                }
            });
        }

        private void DatabaseStateChanged()
        {
            IsDatabaseSet = !string.IsNullOrEmpty(PathCollection.Database_Pos);
            RefreshCheckmarkState();
        }

        private void RefreshCheckmarkState()
        {
            bool dbIsSet = !string.IsNullOrEmpty(PathCollection.Database_Pos);
            if(dbIsSet && State != Icon_Checkmark)
            {
                State = Icon_Checkmark;
            }
            else if(!dbIsSet && State != Icon_X)
            {
                State = Icon_X;
            }
        }
    }
}

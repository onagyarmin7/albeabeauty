﻿using AlbeaBeauty.Logic.Database;
using AlbeaBeauty.Utils;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using System.Windows.Controls;
using System.Windows.Input;

namespace AlbeaBeauty.VM
{
    public class MainVM : ViewModelBase
    {
        public ICommand OpenWindow { get; set; }
        public ICommand SetDB { get; set; }

        private UserControl _content;
        public UserControl ContentWindow
        {
            get { return _content; }
            set { Set(ref _content, value); }
        }

        public bool DatabaseIsSet
        {
            get
            {
                return !string.IsNullOrEmpty(PathCollection.Database_Pos);
            }
        }

        private IMainLogic MainLogic;

        public MainVM(IMainLogic logic)
        {
            MainLogic = logic;

            OpenWindow = new RelayCommand<UserControl>((window) => 
            {          
                if (window != null)
                {
                    this.ContentWindow = window;
                }
            });

            SetDB = new RelayCommand(() =>
            {
                try
                {
                    PathCollection.Database_Pos = MainLogic.GetDatabase();
                }
                catch (FileNotFoundException)
                { }       
            });
        }
    }
}

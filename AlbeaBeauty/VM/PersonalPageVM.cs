﻿using AlbeaBeauty.Logic.PersonalPage;
using AlbeaBeauty.Utils;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using static AlbeaBeauty.Utils.StructCollection;

namespace AlbeaBeauty.VM
{
    class PersonalPageVM : ViewModelBase
    {     
        private PersonalData currentPerson;
        public PersonalData CurrentPerson
        {
            get { return currentPerson; }
            set { Set(ref currentPerson, value); }
        }

        public ICommand AddNewTreatment;
        public ICommand AddOldTreatment;
        public ICommand CreatePerson;

        private List<string> treatments = new List<string>() { "<Kezelések>","Sminktetoválás" };
        public List<string> Treatments { get { return treatments; } }

        private List<string> treatmentTypes = new List<string>() { "Szemöldök - nano szálas", "Ajak - félsatír" };
        public List<string> Types { get { return treatmentTypes; } }

        private IPersonalPageLogic logic;
        

        public PersonalPageVM(IPersonalPageLogic logic)
        {
            this.logic = logic;

            AddNewTreatment = new RelayCommand<NewTreatment>((info) =>
            {
                this.logic.AddNewTreatment(info.parent, info.name, info.details);
            });

            AddOldTreatment = new RelayCommand<OldTreatment>((info) =>
            {
                this.logic.AddEarlierTreatments(info.parent, info.tDetails);
            });

            CreatePerson = new RelayCommand<int>((id) =>
            {
                BitmapImage bim = new BitmapImage();
                try
                {
                    bim.BeginInit();
                    bim.UriSource = new Uri(System.IO.Path.GetFullPath(PathCollection.Images_Folder + id + ".jpeg")
                        , UriKind.RelativeOrAbsolute);
                    bim.EndInit();
                }
                catch (Exception)
                {
                    bim = new BitmapImage(new Uri("pack://application:,,,/Resources/profile_man.jpg"));
                }


                CurrentPerson = new PersonalData() 
                { 
                    image = bim,
                    name = "Olajos Tibi",
                    date = "2021.01.01",
                    phone = "+36305264463",
                    email = "tarzanlaci@gmail.com",
                    facebook = "www.facebook.com/tarzan.laszlo"
                };

            });
        }
    }
}

﻿using AlbeaBeauty.Logic;
using AlbeaBeauty.Logic.Database;
using AlbeaBeauty.Utils;
using AlbeaBeauty.VM;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AlbeaBeauty.View
{
    /// <summary>
    /// Interaction logic for DBSetUC.xaml
    /// </summary>
    public partial class DBSetUC : UserControl
    {
        public DBSetUC()
        {
            InitializeComponent();
            InitSettings();
        }

        private DBSetVM vm;
        private DBSetVM GetViewModel
        {
            get
            {
                return vm ?? (vm = this.DataContext as DBSetVM);
            }
        }

        private void InitSettings()
        {
            this.DataContext = new DBSetVM(new DBLogic());

            GetViewModel.DatabasePosChangedEvent += DBSetUC_DatabasePosChangedEvent;
            tb_position.Text = PathCollection.Database_Pos;
        }

        private void DBSetUC_DatabasePosChangedEvent()
        {
            tb_position.Text = PathCollection.Database_Pos;
        }

        private void Browse_Click(object sender, RoutedEventArgs e)
        {
            GetViewModel.BrowseFile.Execute(null);
        }

        private void NewDatabase_Click(object sender, RoutedEventArgs e)
        {
            GetViewModel.CreateNewDB.Execute(null);
        }
    }
}

﻿using AlbeaBeauty.Logic.PersonalPage;
using AlbeaBeauty.View.TreatmentHandlers;
using AlbeaBeauty.VM;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using static AlbeaBeauty.Utils.StructCollection;
using static AlbeaBeauty.VM.PersonalPageVM;

namespace AlbeaBeauty.View
{
    /// <summary>
    /// Interaction logic for PersonalPageUC.xaml
    /// </summary>
    public partial class PersonalPageUC : UserControl
    {
        public PersonalPageUC(int id)
        {
            InitializeComponent();
            InitSettings(id);
        }

        private int CurrentID;

        private PersonalPageVM vm;
        private PersonalPageVM GetViewmodel
        {
            get { return vm ?? (vm = this.DataContext as PersonalPageVM); }
        }

        private void InitSettings(int id)
        {
            this.DataContext = new PersonalPageVM(new PersonalPageLogic());
            CurrentID = id;

            GetViewmodel.CreatePerson?.Execute(CurrentID);

            Dictionary<string, string> treatments = new Dictionary<string, string>()
            {
                { "Dátum", "2021.01.01."},
                { "Szem", "Bal"},
                { "Egyéb", "asgasgasgasgagas"},
            };

            try
            {
                GetViewmodel.AddNewTreatment?.Execute(new NewTreatment()
                {
                    parent = panel_addnew,
                    name = "PMU",
                    details = new List<string> { "Dátum", "Szem", "Szemöldök", "Száj", "Pigment", "Technika", "Megjegyzés", "Technika", "Technika", "Technika" }
                });

                GetViewmodel.AddOldTreatment?.Execute(new OldTreatment()
                {
                    parent = panel_addold,
                    tDetails = treatments,
                });
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }  
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if((sender as ComboBox).SelectedIndex == 0)
            {
                var window = new TreatmentManagerWindow(CurrentID);
                window.ShowDialog();
            }
        }
    }
}

﻿using AlbeaBeauty.Logic.GuestList;
using AlbeaBeauty.VM;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using static AlbeaBeauty.Utils.StructCollection;

namespace AlbeaBeauty.View
{
    /// <summary>
    /// Interaction logic for GuestListUC.xaml
    /// </summary>
    public partial class GuestListUC : UserControl
    {
        public event Action<int> OpenProfile;

        public GuestListUC()
        {
            InitializeComponent();
            InitSettings();
        }

        private void InitSettings()
        {
            this.DataContext = new GuestListVM(new GuestListLogic());
        }

        private void OpenProfile_Click(object sender, RoutedEventArgs e)
        {
            int id = 0;
            OpenProfile?.Invoke(0);
        }

        private void NewGuest_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                (this.DataContext as GuestListVM).AddUser.Execute(new AddUserStruct()
                {
                    panel = panel_main,
                    data = new PersonalData()
                    {
                        name = "Tarzan László",
                        date = "1976.05.01",
                        phone = "+368278211",
                        email = "tarlaci@gmail.com",
                        facebook = "www.facebook.com/tarzanlaci"
                    },
                    id = 1,
                });
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }        
        }
    }
}

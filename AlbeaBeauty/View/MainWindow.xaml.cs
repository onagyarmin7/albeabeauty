﻿using AlbeaBeauty.Logic.Database;
using AlbeaBeauty.Utils;
using AlbeaBeauty.View;
using AlbeaBeauty.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AlbeaBeauty
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private MainUC mainPage;
        private MainUC GetMainPage
        {
            get
            {
                return mainPage ?? (mainPage = new MainUC());
            }
        }

        private MainVM vm;
        private MainVM GetViewModel
        {
            get
            {
                return vm ?? (vm = this.DataContext as MainVM);
            }
        }

        public MainWindow()
        {
            InitializeComponent();
            InitSettings();   
        }

        private void InitSettings()
        {
            this.DataContext = new MainVM(new MainLogic());
            GetViewModel.OpenWindow.Execute(GetMainPage);
            GetViewModel.SetDB.Execute(null);           
        }

        private void OpenGuestList_Click(object sender, RoutedEventArgs e)
        {
            if (!GetViewModel.DatabaseIsSet) return;

            var guestList = new GuestListUC();
            guestList.OpenProfile += (id) => {
                GetViewModel.OpenWindow.Execute(new PersonalPageUC(id)); 
            };

            GetViewModel.OpenWindow.Execute(guestList);
        }

        private void Image_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            GetViewModel.OpenWindow.Execute(GetMainPage);
        }

        private void OpenDBSet_Click(object sender, RoutedEventArgs e)
        {
            GetViewModel.OpenWindow.Execute(new DBSetUC());
        }

        private void OpenNotifications_Click(object sender, RoutedEventArgs e)
        {
            if (!GetViewModel.DatabaseIsSet) return;

            GetViewModel.OpenWindow.Execute(new NotificationUI());
        }
    } 
}

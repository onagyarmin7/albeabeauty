﻿using AlbeaBeauty.VM.TreatmentHandlers;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using static AlbeaBeauty.Utils.StructCollection;

namespace AlbeaBeauty.View.TreatmentHandlers
{
    /// <summary>
    /// Interaction logic for TreatmentSettings.xaml
    /// </summary>
    public partial class TreatmentSettings : UserControl
    {
        public Action GoBack;

        public TreatmentSettings()
        {
            InitializeComponent();       

            Style itemContainerStyle = new Style(typeof(ListBoxItem));
            itemContainerStyle.Setters.Add(new Setter(ListBoxItem.AllowDropProperty, true));
            itemContainerStyle.Setters.Add(new EventSetter(ListBoxItem.PreviewMouseMoveEvent, new MouseEventHandler(lb_main_PreviewMouseMove)));
            itemContainerStyle.Setters.Add(new EventSetter(ListBoxItem.DropEvent, new DragEventHandler(lb_main_Drop)));
            lb_main.ItemContainerStyle = itemContainerStyle;
        }

        private void lb_main_PreviewMouseMove(object sender, MouseEventArgs e)
        {
            if (sender is ListBoxItem && e.LeftButton == MouseButtonState.Pressed)
            {
                ListBoxItem draggedItem = sender as ListBoxItem;
                DragDrop.DoDragDrop(draggedItem, draggedItem.DataContext, DragDropEffects.Move);
                draggedItem.IsSelected = true;
            }
        }

        private void lb_main_Drop(object sender, DragEventArgs e)
        {
            var droppedData = e.Data.GetData(typeof(string)) as string;
            var target = ((ListBoxItem)(sender)).DataContext as string;

            int removedIdx = lb_main.Items.IndexOf(droppedData);
            int targetIdx = lb_main.Items.IndexOf(target);

            (this.DataContext as TreatmentSettingsVM).DragCommand?.Execute(new DragNDropInfo()
            {
                droppedData = droppedData,
                target = target,
                removedIdx = removedIdx,
                targetIdx = targetIdx,
            });

            lb_main.Items.Refresh();
        }

        private void GoBack_Click(object sender, RoutedEventArgs e)
        {
            this.GoBack?.Invoke();
        }
    }
}

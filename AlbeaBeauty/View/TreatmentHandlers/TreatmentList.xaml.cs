﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AlbeaBeauty.View.TreatmentHandlers
{
    /// <summary>
    /// Interaction logic for TreatmentList.xaml
    /// </summary>
    public partial class TreatmentList : UserControl
    {
        public Action OpenTreatmentSetter;

        public TreatmentList()
        {
            InitializeComponent();
        }

        private void NewTreatment_Click(object sender, RoutedEventArgs e)
        {
            this.OpenTreatmentSetter.Invoke();
        }

        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            Window.GetWindow(this).Close();
        }
    }
}

using System;
using System.IO;
using System.Data.SQLite;

namespace AlbeaBeauty.Repository
{
    public class Repository : IRepository
    {
        public string CreateDatabase(string folderPath)
        {
            string dbLocation = string.Empty;

            if(!string.IsNullOrEmpty(folderPath))
            {
                dbLocation = folderPath + "\\AlbeaData.sqlite";
                if(File.Exists(dbLocation))
                {
                    File.Delete(dbLocation);
                }

                SQLiteConnection.CreateFile(dbLocation);
                CreateGuestsTable(dbLocation);
            }

            return dbLocation;
        }

        private void CreateGuestsTable(string dbLocation)
        {
            using (SQLiteConnection con = new SQLiteConnection($"Data Source={dbLocation};Version=3;"))
            {
                con.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(con))
                {
                    cmd.CommandText = "CREATE TABLE Users(ID INT PRIMARY KEY," +
                        "Picture VARCHAR(40)," +
                        "Name VARCHAR(50)," +
                        "DateOfBirth VARCHAR(11)," +
                        "Phone VARCHAR(20)," +
                        "Email VARCHAR(20)," +
                        "Facebook VARCHAR(20));";

                    cmd.ExecuteNonQuery();
                }
            }
        }
    }
}

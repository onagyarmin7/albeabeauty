﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Controls;
using System.Windows.Media.Imaging;

namespace AlbeaBeauty.Utils
{
    public class StructCollection
    {
        public struct NewTreatment
        {
            public Grid parent;
            public string name;
            public List<string> details;
        }

        public struct OldTreatment
        {
            public DockPanel parent;
            public Dictionary<string, string> tDetails;
        }

        public struct PersonalData
        {
            public BitmapImage image { get; set; }
            public string name { get; set; }
            public string date { get; set; }
            public string phone { get; set; }
            public string email { get; set; }
            public string facebook { get; set; }
        }

        public struct DragNDropInfo
        {
            public string droppedData { get; set; }
            public string target { get; set; }
            public int removedIdx { get; set; }
            public int targetIdx { get; set; }
        }
        
        public struct AddUserStruct
        {
            public StackPanel panel { get; set; }
            public PersonalData data { get; set; }
            public int id { get; set; }
        }
    }
}

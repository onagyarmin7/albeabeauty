﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AlbeaBeauty.Utils
{
    public static class PathCollection
    {
        public static string Database_Pos { get; set; }
        public const string PathFile = "Resources/path.ini";
        public const string Resources_Folder = "Resources/";
        public const string Images_Folder = Resources_Folder + "Images/";
    }
}

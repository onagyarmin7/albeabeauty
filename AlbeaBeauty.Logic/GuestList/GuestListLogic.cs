﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Controls;
using static AlbeaBeauty.Utils.StructCollection;

namespace AlbeaBeauty.Logic.GuestList
{
    public class GuestListLogic : IGuestListLogic
    {
        public void AddNewGuest(StackPanel panel, PersonalData data, int id)
        {
            Grid grid = GuestBuilder.GetDefaultGrid();
            GuestBuilder.UploadGridWithData(grid, data);
            panel.Children.Add(grid);
            panel.UpdateLayout();

            GC.WaitForPendingFinalizers();
            GC.Collect();
        }
    }
}

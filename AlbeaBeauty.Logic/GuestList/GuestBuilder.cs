﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using static AlbeaBeauty.Utils.StructCollection;

namespace AlbeaBeauty.Logic.GuestList
{
    static class GuestBuilder
    {
        public static void UploadGridWithData(Grid grid, PersonalData data)
        {
            Image image = new Image()
            {
                Source = new BitmapImage(new Uri("pack://application:,,,/Resources/male_example.jpeg")),
                Stretch = Stretch.Fill,
                MaxHeight = 30,
                MaxWidth = 50
            };
            Grid.SetColumn(image, 0);

            Label l1 = new Label()
            {
                Content = data.name,
                HorizontalContentAlignment = HorizontalAlignment.Center,
                Background = new SolidColorBrush(Color.FromRgb(245, 212, 207)),
            };
            Grid.SetColumn(l1, 1);

            Label l2 = new Label()
            {
                Content = data.date,
                HorizontalContentAlignment = HorizontalAlignment.Center,
            };
            Grid.SetColumn(l2, 2);

            Label l3 = new Label()
            {
                Content = data.phone,
                HorizontalContentAlignment = HorizontalAlignment.Center,
                Background = new SolidColorBrush(Color.FromRgb(245, 212, 207)),
            };
            Grid.SetColumn(l3, 3);

            Label l4 = new Label()
            {
                Content = data.email,
                HorizontalContentAlignment = HorizontalAlignment.Center,
            };
            Grid.SetColumn(l4, 4);

            Label l5 = new Label()
            {
                Content = data.facebook,
                HorizontalContentAlignment = HorizontalAlignment.Center,
                Background = new SolidColorBrush(Color.FromRgb(245, 212, 207)),
            };
            Grid.SetColumn(l5, 5);

            Button button = new Button()
            {
                Content = "Bővebb",
                HorizontalContentAlignment = HorizontalAlignment.Center,
                BorderThickness = new Thickness(0),
                MaxWidth = 300,
                Margin = new Thickness(5, 0, 0, 0),
            };
            Grid.SetColumn(button, 6);

            grid.Children.Add(image);
            grid.Children.Add(l1);
            grid.Children.Add(l2);
            grid.Children.Add(l3);
            grid.Children.Add(l4);
            grid.Children.Add(l5);
            grid.Children.Add(button);
        }

        public static Grid GetDefaultGrid()
        {
            Grid grid = new Grid() { Margin = new Thickness(0, 0, 0, 10), };

            grid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Auto) });
            for (int i = 0; i < 6; i++)
            {
                grid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) });
            }

            return grid;
        }
    }
}

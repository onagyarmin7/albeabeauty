﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Controls;
using static AlbeaBeauty.Utils.StructCollection;

namespace AlbeaBeauty.Logic.GuestList
{
    public interface IGuestListLogic
    {
        void AddNewGuest(StackPanel panel, PersonalData data, int id); 
    }
}

using AlbeaBeauty.Utils;
using System;
using System.IO;

namespace AlbeaBeauty.Logic.Database
{
    public class MainLogic : IMainLogic
    {
        public string GetDatabase()
        {
            IniFile.PathFile = new IniFile(PathCollection.PathFile);
            var db = IniFile.PathFile.Read("Database");

            if(!string.IsNullOrEmpty(db) && File.Exists(db))
            {
                return db;
            }

            throw new FileNotFoundException("Database is missing.");
        }
    }
}

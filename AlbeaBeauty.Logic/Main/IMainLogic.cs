﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Controls;

namespace AlbeaBeauty.Logic.Database
{
    public interface IMainLogic
    {
        string GetDatabase();
    }
}

﻿using AlbeaBeauty.Repository;
using AlbeaBeauty.Utils;
using Microsoft.Win32;
using Microsoft.WindowsAPICodePack.Dialogs;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace AlbeaBeauty.Logic.Database
{
    public class DBLogic : IDBLogic
    {

        public DBLogic(IRepository repository = null)
        {
            this.Repository = repository ?? new AlbeaBeauty.Repository.Repository();
        }

        private IRepository Repository;

        public bool SetDatabase(string path)
        {
            if(!string.IsNullOrEmpty(path) && File.Exists(path) && path.EndsWith(".sqlite"))
            {
                PathCollection.Database_Pos = path;
                IniFile.PathFile.Write("Database", path);

                return true;
            }

            return false;
        }

        public bool CreateDB()
        {
            string dbLocation = Repository.CreateDatabase(BrowseFolder());
            return SetDatabase(dbLocation);
        }

        public string BrowseFile()
        {
            string filename = string.Empty;
            OpenFileDialog ofd = new OpenFileDialog();
            if (!string.IsNullOrEmpty(PathCollection.Database_Pos))
            {
                ofd.InitialDirectory = Directory.GetParent(PathCollection.Database_Pos).FullName;
            }

            if (ofd.ShowDialog() == true)
            {
                filename = ofd.FileName;
            }

            return filename;       
        }

        private string BrowseFolder()
        {
            string folder = string.Empty;

            using (var cfd = new CommonOpenFileDialog())
            {
                cfd.IsFolderPicker = true;
                if (cfd.ShowDialog() == CommonFileDialogResult.Ok)
                {
                    folder = cfd.FileName;
                }
            }

            return folder;
        }
    }
}

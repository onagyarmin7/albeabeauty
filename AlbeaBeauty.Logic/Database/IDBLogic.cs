﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AlbeaBeauty.Logic.Database
{
    public interface IDBLogic
    {
        public static string Database_Pos { get; set; }

        bool SetDatabase(string path);
        bool CreateDB();
        string BrowseFile();
    }
}

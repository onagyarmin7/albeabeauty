﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Controls;

namespace AlbeaBeauty.Logic.PersonalPage
{
    public interface IPersonalPageLogic
    {
        void AddNewTreatment(Grid parent, string name, List<string> details);
        void AddEarlierTreatments(DockPanel parent, Dictionary<string, string> treatmentDetails);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;

namespace AlbeaBeauty.Logic.PersonalPage
{
    static class NewItemBuilder
    {
        public static void RemoveChildrenAndRowDef(int count, Grid grid)
        {
            grid.RowDefinitions.Clear();
            grid.Children.Clear();

            for (int i = 0; i < count; i++)
            {
                grid.RowDefinitions.Add(new RowDefinition()
                {
                    Height = new GridLength(1, GridUnitType.Star),
                });
            }
        }
    }
}

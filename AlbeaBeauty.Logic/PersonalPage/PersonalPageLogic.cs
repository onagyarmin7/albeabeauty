﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;

namespace AlbeaBeauty.Logic.PersonalPage
{
    public class PersonalPageLogic : IPersonalPageLogic
    {
        public void AddNewTreatment(Grid parent, string name, List<string> details)
        {
            NewItemBuilder.RemoveChildrenAndRowDef(details.Count, parent);

            for (int i = 0; i < details.Count; i++)
            {
                Label lb = new Label()
                {
                    Content = details[i] + ":",
                    HorizontalContentAlignment = HorizontalAlignment.Left,
                    VerticalContentAlignment = VerticalAlignment.Center,
                    Margin = new Thickness(5, 0, 0, 5),
                };
                Grid.SetRow(lb, i);
                parent.Children.Add(lb);

                var tb = new TextBox()
                {
                    HorizontalContentAlignment = HorizontalAlignment.Left,
                    VerticalContentAlignment = VerticalAlignment.Center,
                    Margin = new Thickness(5, 0, 15, 5),
                    MinWidth = 200,
                };
                Grid.SetRow(tb, i);
                Grid.SetColumn(tb, 1);
                parent.Children.Add(tb);
            }
        }

        public void AddEarlierTreatments(DockPanel parent, Dictionary<string, string> treatmentDetails)
        {
            Border border = OldItemBuilder.GetBorder();
            Grid grid = OldItemBuilder.GetGrid(treatmentDetails.Count);

            int rowCounter = 0;
            foreach (var detail in treatmentDetails)
            {
                Label label = OldItemBuilder.GetLabel(detail.Key, rowCounter);
                TextBox tb = OldItemBuilder.GetTextbox(detail.Value, rowCounter);
                grid.Children.Add(label);
                grid.Children.Add(tb);

                ++rowCounter;
            }

            grid.Children.Add(OldItemBuilder.GetImagesPanel(treatmentDetails.Count));
            border.Child = grid;

            parent.Children.Add(border);
        }
    }
}

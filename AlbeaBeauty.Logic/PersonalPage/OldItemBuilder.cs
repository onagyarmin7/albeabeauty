﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace AlbeaBeauty.Logic.PersonalPage
{
    public static class OldItemBuilder
    {
        public static DockPanel GetImagesPanel(int rows)
        {
            DockPanel panel = new DockPanel();
            Grid.SetColumn(panel, 2);
            Grid.SetRowSpan(panel, rows);

            panel.Children.Add(GetImageButton(@"/AlbeaBeauty;component/Resources/x.png", 30));
            panel.Children.Add(GetImageButton(@"/AlbeaBeauty;component/Resources/modify.png", 20));

            return panel;
        }

        public static Label GetLabel(string value, int counter)
        {
            Label label = new Label()
            {
                Content = value,
                BorderBrush = Brushes.Black,
                BorderThickness = new Thickness(1),
                FontWeight = FontWeights.Bold,
                HorizontalContentAlignment = HorizontalAlignment.Center,
                VerticalContentAlignment = VerticalAlignment.Center,
            };
            Grid.SetRow(label, counter);

            return label;
        }

        public static TextBox GetTextbox(string value, int row)
        {
            TextBox tb = new TextBox()
            {
                Text = value,
                HorizontalContentAlignment = HorizontalAlignment.Center,
                VerticalContentAlignment = VerticalAlignment.Center,
                IsEnabled = false,
            };

            Grid.SetRow(tb, row);
            Grid.SetColumn(tb, 1);

            return tb;
        }

        public static Border GetBorder()
        {
            Border border = new Border()
            {
                BorderThickness = new Thickness(2),
                BorderBrush = Brushes.Pink,
                Margin = new Thickness(10, 5, 40, 5),
            };
            DockPanel.SetDock(border, Dock.Top);
            Grid.SetColumnSpan(border, 2);
            Grid.SetRow(border, 3);

            return border;
        }

        public static Grid GetGrid(int count)
        {
            Grid grid = new Grid();

            grid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) });
            grid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(2, GridUnitType.Star) });
            grid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Auto) });

            for (int i = 0; i < count; i++)
            {
                grid.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(1, GridUnitType.Auto) });
            }

            return grid;
        }

        private static Button GetImageButton(string source, int height)
        {
            BitmapImage logo = new BitmapImage();
            logo.BeginInit();
            logo.UriSource = new Uri(source, UriKind.Relative);
            logo.EndInit();

            Image image = new Image()
            {
                Source = logo,
                MaxHeight = height,
            };

            Button button = new Button()
            {
                Background = Brushes.White,
                BorderBrush = Brushes.White,
                Content = image,
            };

            DockPanel.SetDock(button, Dock.Top);

            return button;
        }
    }
}
